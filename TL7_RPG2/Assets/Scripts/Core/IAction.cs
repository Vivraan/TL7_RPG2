﻿namespace TL7.Core
{
    public interface IAction
    {
        void StartAction(params object[] args);
        void CancelAction();
    }
}
