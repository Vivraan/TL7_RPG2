﻿using UnityEngine;

namespace TL7.Core
{
    public class CameraFacing : MonoBehaviour
    {
        // Update is called once per frame
        void LateUpdate() => transform.forward = Camera.main.transform.forward;
    }
}
