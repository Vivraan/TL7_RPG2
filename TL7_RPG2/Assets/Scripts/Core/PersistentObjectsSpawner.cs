﻿using UnityEngine;

namespace TL7.Core
{
    public class PersistentObjectsSpawner : MonoBehaviour
    {
        [SerializeField] GameObject persistentObjectPrefab = null;
        static bool spawned = false;

        void Awake()
        {
            if (!spawned)
            {
                DontDestroyOnLoad(Instantiate(persistentObjectPrefab));
                spawned = true;
            }
        }
    }
}
