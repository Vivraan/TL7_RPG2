﻿using UnityEngine;

namespace TL7.Core
{
    public class ActionScheduler : MonoBehaviour
    {
        IAction currentAction;

        public void StartAction(in IAction action)
        {
            if (currentAction != action)
            {
                currentAction?.CancelAction();
                currentAction = action;
            }
        }

        public void CancelCurrentAction() => StartAction(null);
    }
}
