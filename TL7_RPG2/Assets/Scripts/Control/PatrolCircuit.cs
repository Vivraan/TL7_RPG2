﻿using UnityEngine;

namespace TL7.Control
{
    public class PatrolCircuit : MonoBehaviour
    {
        [SerializeField] Color vertexColor = Color.grey;
        [SerializeField] Color edgeColor = Color.grey;
        [SerializeField] Color vertexColorSelected = Color.blue;
        [SerializeField] Color edgeColorSelected = Color.green;
        [SerializeField] Mesh vertexVizMesh = null;

        public Vector3 this[int index] => transform.GetChild(index).position;

        private void OnDrawGizmos()
        {
            DrawEdge(0, connectEnd: true);
            for (var i = 1; i < transform.childCount; i++)
            {
                DrawEdge(i);
            }
        }

        private void OnDrawGizmosSelected()
        {
            DrawEdge(0, connectEnd: true, selected: true);
            for (var i = 1; i < transform.childCount; i++)
            {
                DrawEdge(i, selected: true);
            }   
        }

        private void DrawEdge(int index, bool connectEnd = false, bool selected = false)
        {
            if (transform.childCount != 0)
            {
                int prevIndex = connectEnd ? transform.childCount - 1 : index - 1;

                Gizmos.color = selected ? vertexColorSelected : vertexColor;
                Gizmos.DrawWireMesh(vertexVizMesh, transform.GetChild(index).position);
                Gizmos.color = selected ? edgeColorSelected : edgeColor;
                Gizmos.DrawLine(transform.GetChild(prevIndex).position, transform.GetChild(index).position);
            }
        }

        public void IncrementWaypointIndex(ref int index) => index = (index == transform.childCount - 1) ? 0 : index + 1;
    }
}
