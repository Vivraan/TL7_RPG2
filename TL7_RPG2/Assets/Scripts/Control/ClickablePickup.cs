﻿using System.Collections;
using System.Collections.Generic;
using TL7.UI;
using GameDevTV.Inventories;
using UnityEngine;

namespace TL7.Control
{
    [RequireComponent(typeof(Pickup))]
    public class ClickablePickup : MonoBehaviour, IRaycastable
    {
        Pickup pickup;

        void Awake()
        {
            pickup = GetComponent<Pickup>();
        }

        public CursorType CursorType => pickup.CanBePickedUp() ? CursorType.Pickup : CursorType.FullPickup;

        public bool TryHandleRaycast(in PlayerController callingController)
        {
            if (Input.GetMouseButtonDown(0))
            {
                pickup.PickupItem();
            }
            return true;
        }
    }
}