﻿using System;
using System.Collections.Generic;
using TL7.Combat;
using TL7.Movement;
using TL7.Attributes;
using TL7.SceneManagement;
using TL7.UI;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Assertions;
using UnityEngine.EventSystems;
using GameDevTV.Inventories;

namespace TL7.Control
{
    [
        RequireComponent(typeof(Inventory)),
        RequireComponent(typeof(Equipment)),
        RequireComponent(typeof(ActionStore)),
    ]
    public class PlayerController : MonoBehaviour
    {
        [Serializable]
        class CursorInfo
        {
            [SerializeField] CursorType type = CursorType.None;
            [SerializeField] Texture2D texture = null;
            [SerializeField] Vector2 hotspot = Vector2.zero;

            public CursorType Type => type;
            public Texture2D Texture => texture;
            public Vector2 Hotspot => hotspot;
        }

        [SerializeField] float gameReloadDelay = 3f;
        [SerializeField] CursorInfo[] cursors = null;
        [SerializeField] float walkableDetectionTolerance = 1f;
        [SerializeField] float sphereCastRadius = 1f;

        [SerializeField] ControlSource controlSource = null;
        private Mover mover;
        private Health health;
        private Fighter fighter;
        private float timeElapsedSinceDeath = 0f;
        private bool draggingUI = false;
        private Dictionary<CursorType, CursorInfo> lookup;
        private Dictionary<CursorType, CursorInfo> Lookup
        {
            get
            {
                BuildLookup();
#if UNITY_EDITOR
                Assert.IsFalse(
                    (lookup == null),
                    $"Oof ouch there was an error with {GetType()}."
                );
#endif
                return lookup;
            }
        }

        private void BuildLookup()
        {
            if (lookup == null)
            {
                lookup = new Dictionary<CursorType, CursorInfo>();
                for (var i = 0; i < cursors.Length; ++i)
                {
                    if (!lookup.ContainsKey(cursors[i].Type))
                    {
                        lookup[cursors[i].Type] = cursors[i];
                    }
                    else
                    {
                        lookup = null;
                        return;
                    }
                }
#if UNITY_EDITOR
                Debug.Log($"Lookup built for {GetType()} '{name}'");
#endif
            }
        }

        void Awake()
        {
            mover = GetComponent<Mover>();
            health = GetComponent<Health>();
            fighter = GetComponent<Fighter>();
        }

        void Update()
        {
            if (InteractWithUI())
            {
                return;
            }
            if (!health.IsDead)
            {
                if (InteractWithRaycastables())
                {
                    return;
                }
                if (InteractWithMovement())
                {
                    return;
                }
            }
            else
            {
                timeElapsedSinceDeath += Time.deltaTime;
                if (timeElapsedSinceDeath >= gameReloadDelay)
                {
                    timeElapsedSinceDeath = 0;
                    StartCoroutine(FindObjectOfType<SavingWrapper>().LoadCheckpoint());
                }
            }
            SetCursor(CursorType.None);
            // If no interactions occur, this confirms that nothing happens.
            // Debug.Log("I got nothin'.");
        }

        private bool InteractWithUI()
        {
            if (Input.GetMouseButtonUp(0))
            {
                draggingUI = false;
            }
            if (EventSystem.current.IsPointerOverGameObject())
            {
                if (Input.GetMouseButtonDown(0))
                {
                    draggingUI = true;
                }
                SetCursor(CursorType.UI);
                return true;
            }
            if (draggingUI)
            {
                return true;
            }
            return false;
        }

        private bool InteractWithRaycastables()
        {
            RaycastHit[] hits = Physics.SphereCastAll(controlSource.ScreenPositionSignal, sphereCastRadius);
            Array.Sort(hits, (a, b) =>
            {
                float distance = a.distance - b.distance;
                return (distance <= Mathf.Epsilon) ? 0 : (a.distance < b.distance) ? -1 : +1;
            });
            for (var i = 0; i < hits.Length; ++i)
            {
                var raycastables = hits[i].transform.GetComponents<IRaycastable>();
                for (var j = 0; j < raycastables.Length; ++j)
                {
                    if (raycastables[j].TryHandleRaycast(this))
                    {
                        SetCursor(DisambiguateCursor(raycastables[j].CursorType));
                        return true;
                    }
                }
            }
            return false;
        }

        private CursorType DisambiguateCursor(in CursorType cursorType)
        {
            if (cursorType == CursorType.Combat)
            {
                return fighter.CurrentWeaponConfig.CursorType;
            }
            return cursorType;
        }

        private bool InteractWithMovement()
        {
            // if (Physics.Raycast(controlSource.ScreenPositionSignal, out RaycastHit hit))
            if (
                TryRaycastOntoNavMesh(out Vector3 location)
                && (mover != null) && mover.CanMoveTo(location)
            )
            {
                if (controlSource.LeftClickSignal)
                {
                    mover.StartAction(location);
                }
                SetCursor(CursorType.Movement);
                return true;
            }
            return false;
        }

        private bool TryRaycastOntoNavMesh(out Vector3 location)
        {
            location = Vector3.zero;
            return Physics.Raycast(controlSource.ScreenPositionSignal, out RaycastHit raycastHit)
                && CanTakeNavPath(raycastHit, out location);
        }

        private bool CanTakeNavPath(in RaycastHit raycastHit, out Vector3 location)
        {
            bool positionSampled = NavMesh.SamplePosition(
                raycastHit.point,
                out NavMeshHit navMeshHit,
                walkableDetectionTolerance,
                NavMesh.AllAreas
            );
            if (positionSampled && mover.CanMoveTo(navMeshHit.position))
            {
                location = navMeshHit.position;
                return true;
            }
            location = Vector3.zero;
            return false;
        }

        private void SetCursor(in CursorType cursorType)
        {
            var cursor = Lookup?[cursorType];
            Cursor.SetCursor(
                cursor.Texture,
                cursor.Hotspot,
                CursorMode.Auto
            );
        }
    }
}
