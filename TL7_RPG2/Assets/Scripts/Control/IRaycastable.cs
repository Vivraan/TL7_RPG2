﻿using TL7.UI;

namespace TL7.Control
{
    public interface IRaycastable
    {
        bool TryHandleRaycast(in PlayerController playerController);
        CursorType CursorType { get; }
    }
}
