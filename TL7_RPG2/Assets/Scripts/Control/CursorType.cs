﻿namespace TL7.UI
{
    [System.Flags]
    public enum CursorType
    {
        None = 0,
        UI = (1 << 0),
        Movement = (1 << 1),
        Sword = (1 << 2),
        Bow = (1 << 3),
        Fire = (1 << 4),
        Combat = Sword | Bow | Fire,
        Pickup = (1 << 5),
        FullPickup = (1 << 6)
    }
}
