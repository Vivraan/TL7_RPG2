﻿using TL7.Combat;
using UnityEngine;

namespace TL7.Audio
{
    [RequireComponent(typeof(AudioSource))]
    public class AudioRandomizer : MonoBehaviour
    {
        [SerializeField] AudioClip[] audioClips = null;
        [SerializeField, Range(-3f, +3f)] float minPitch = .9f;
        [SerializeField, Range(-3f, +3f)] float maxPitch = 1.1f;
        private AudioSource audioSource;

        void Awake()
        {
            audioSource = GetComponent<AudioSource>();
        }

        void Start()
        {
            Random.InitState(gameObject.GetInstanceID() ^ GetInstanceID() ^ System.DateTime.Now.Millisecond);
        }

        public void Play()
        {
            if (audioClips.Length > 0)
            {
                audioSource.pitch = Random.Range(minPitch, maxPitch);
                audioSource.clip = audioClips[Random.Range(0, audioClips.Length - 1)];
                audioSource.Play();
            }
        }

        public void PlayDelayed(float delay)
        {
            if (audioClips.Length > 0)
            {
                audioSource.pitch = Random.Range(minPitch, maxPitch);
                audioSource.clip = audioClips[Random.Range(0, audioClips.Length - 1)];
                audioSource.PlayDelayed(delay);
            }
        }

        public void PlayOneShot(AudioClip clip)
        {
            if (audioClips.Length > 0)
            {
                audioSource.pitch = Random.Range(minPitch, maxPitch);
                audioSource.PlayOneShot(clip);
            }
        }

        public void PlayScheduled(double time)
        {
            if (audioClips.Length > 0)
            {
                audioSource.pitch = Random.Range(minPitch, maxPitch);
                audioSource.clip = audioClips[Random.Range(0, audioClips.Length - 1)];
                audioSource.PlayScheduled(time);
            }
        }
    }
}
