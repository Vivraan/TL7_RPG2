﻿using GameDevTV.Saving;
using UnityEngine;
using UnityEngine.Playables;

namespace TL7.Cinematic
{
    [RequireComponent(typeof(SaveableEntity))]
    public class CinematicTrigger : MonoBehaviour, ISaveable
    {
        [SerializeField] bool skipCinematic = false;
        private PlayableDirector playableDirector;


        void Awake()
        {
            playableDirector = GetComponent<PlayableDirector>();
        }

        void OnTriggerEnter(Collider other)
        {
            if (other.CompareTag("Player") && !skipCinematic)
            {
                playableDirector.Play();
                GetComponent<BoxCollider>().enabled = false;
                skipCinematic = true;
            }
        }

        void Update()
        {
            if (Input.GetAxis("Cancel") > 0)
            {
                playableDirector.Stop();
            }
        }

        object ISaveable.CaptureState()
        {
            return skipCinematic;
        }

        void ISaveable.RestoreState(object state)
        {
            skipCinematic = (bool)state;
        }
    }
}
