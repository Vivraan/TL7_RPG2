﻿using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEngine.Assertions;
#endif

namespace TL7.Stats
{
    using StatTypeColumn = Dictionary<StatType, float[]>;
    using ProgressionMatrixImpl = Dictionary<CharacterClass, Dictionary<StatType, float[]>>;

    [CreateAssetMenu(fileName = "Progression", menuName = "TL7/Stats/New Progression", order = 0)]
    public class ProgressionMatrix : ScriptableObject
    {
        [System.Serializable]
        public class StatProgression
        {
            [SerializeField] StatType statType = StatType.Health;
            [Tooltip("Levels are 0 indexed.")]
            [SerializeField] float[] levels = null;

            public StatType StatType => statType;
            public float[] Levels => levels;
        }

        [System.Serializable]
        public class ClassProgression
        {
            [SerializeField] CharacterClass characterClass = CharacterClass.Grunt;
            [SerializeField] StatProgression[] stats = null;

            public CharacterClass CharacterClass => characterClass;
            public StatProgression[] Stats => stats;
        }

        [SerializeField] ClassProgression[] classes = null;
        ProgressionMatrixImpl lookup;

        ProgressionMatrixImpl Lookup
        {
            get
            {
                BuildLookup();
#if UNITY_EDITOR
                Assert.IsFalse(
                    (lookup == null),
                    $"Oof ouch there was an error with {GetType()}."
                );
#endif
                return lookup;
            }
        }

        // Allows separation of indices in multiple ways: 
        // X[i, j, k] or X[i][j][k] (or even X[i, j][k] but *not* X[i][j, k])

        public StatTypeColumn this[in CharacterClass i] => Lookup[i];

        public float[] this[in CharacterClass i, in StatType j] => this[i][j];

        public float this[in CharacterClass i, in StatType j, in int k] =>
            0 <= k && k < this[i, j].Length ? this[i, j][k] : 0f;


        private void BuildLookup()
        {
            if (lookup == null)
            {
                lookup = new ProgressionMatrixImpl();
                for (var i = 0; i < classes.Length; ++i)
                {
                    var statColumn = new StatTypeColumn();
                    // Don't start the second loop if the character class already exists
                    if (!lookup.ContainsKey(classes[i].CharacterClass))
                    {
                        for (var j = 0; j < classes[i].Stats.Length; ++j)
                        {
                            if (!statColumn.ContainsKey(classes[i].Stats[j].StatType))
                            {
                                statColumn[classes[i].Stats[j].StatType] = classes[i].Stats[j].Levels;
                            }
                            else
                            {
                                // You have failed me. Begone!
                                lookup = null;
                                return;
                            }
                        }
                        lookup[classes[i].CharacterClass] = statColumn;
                    }
                    else
                    {
                        // You have failed me. Begone!
                        lookup = null;
                        return;
                    }
                }
#if UNITY_EDITOR
                Debug.Log($"Lookup built for {GetType()} '{name}'");
#endif
            }
        }
    }
}
