﻿using System;
using TL7.Attributes;
using GameDevTV.Saving;
using UnityEngine;

namespace TL7.Stats
{
    [RequireComponent(typeof(SaveableEntity))]
    public class BaseStats : MonoBehaviour, ISaveable
    {
        public event Action LevelledUp;
        [SerializeField, Range(0, 99)] int startingLevel = 0;
        [SerializeField] CharacterClass characterClass = CharacterClass.Grunt;
        [SerializeField] ProgressionMatrix progressionMatrix = null;
        [SerializeField] ParticleSystem levelUpParticles = null;
        private Experience experience;
        private int currentLevel = 0;

        public int Level => currentLevel;

        void Awake()
        {
            experience = GetComponent<Experience>();
            currentLevel = CalculateLevel();
        }

        void OnEnable()
        {
            if (experience != null)
            {
                experience.XPClaimed += UpdateLevel;
            }
        }

        void OnDisable()
        {
            if (experience != null)
            {
                experience.XPClaimed -= UpdateLevel;
            }
        }

        private void UpdateLevel()
        {
            int newLevel = CalculateLevel();
            if (currentLevel < newLevel)
            {
                currentLevel = newLevel;
                LevelledUp();
                if (levelUpParticles != null)
                {
                    Instantiate(levelUpParticles, transform);
                }
            }
        }

        private int CalculateLevel()
        {
            if (experience == null)
            {
                return startingLevel;
            }
            int numLevels = BaseLevelsOf(StatType.ExperienceToLevelUp).Length;
            for (var level = 0; level < numLevels; ++level)
            {
                if (BaseByLevel(StatType.ExperienceToLevelUp, level) > experience.XP)
                {
                    return level;
                }
            }
            // The next level to all enumerated is n+1, for n levels
            return numLevels;
        }

        public float[] BaseLevelsOf(in StatType statType) =>
            progressionMatrix[this.characterClass, statType];

        public float BaseByLevel(in StatType statType, in int level) =>
            progressionMatrix[this.characterClass, statType, level];

        public float Base(in StatType statType) =>
            BaseByLevel(statType, Level);

        public float FromAdditives(in StatType statType)
        {
            float total = 0f;
            var providers = GetComponents<IModifierProvider>();
            for (var i = 0; i < providers.Length; ++i)
            {
                foreach (float modifier in providers[i].AdditivesFor(statType))
                {
                    total += modifier;
                }
            }
            return total;
        }

        public float FromMultipliers(in StatType statType)
        {
            float total = 0f;
            var providers = GetComponents<IModifierProvider>();
            for (var i = 0; i < providers.Length; ++i)
            {
                foreach (float modifier in providers[i].MultipliersFor(statType))
                {
                    total += modifier;
                }
            }
            return total;
        }

        public float Of(in StatType statType)
        {
            return (Base(statType) + FromAdditives(statType)) * (1 + FromMultipliers(statType));
        }

        object ISaveable.CaptureState()
        {
            return currentLevel;
        }

        void ISaveable.RestoreState(object state)
        {
            currentLevel = (int)state;
        }
    }
}
