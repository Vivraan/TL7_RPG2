﻿using System.Collections.Generic;

namespace TL7.Stats
{
    public interface IModifierProvider
    {
        IEnumerable<float> AdditivesFor(StatType statType);
        IEnumerable<float> MultipliersFor(StatType statType);
    }
}
