namespace TL7.Stats
{
    public enum CharacterClass
    {
        Player,
        Grunt,
        Archer,
        Mage,
        Boss
    }
}
