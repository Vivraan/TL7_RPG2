﻿using TL7.Control;
using TL7.Attributes;
using TL7.UI;
using UnityEngine;

namespace TL7.Combat
{
    [RequireComponent(typeof(Health))]
    public class CombatTarget : MonoBehaviour, IRaycastable
    {
        CursorType IRaycastable.CursorType => CursorType.Combat;

        bool IRaycastable.TryHandleRaycast(in PlayerController playerController)
        {
            var fighter = playerController.GetComponent<Fighter>();
            if ((fighter != null) && fighter.CanTarget(gameObject))
            {
                if (Input.GetMouseButton(0))
                {
                    fighter.StartAction(gameObject);
                }
                return true;
            }
            return false;
        }
    }
}