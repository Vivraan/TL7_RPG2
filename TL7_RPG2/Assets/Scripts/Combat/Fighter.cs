﻿using System.Collections.Generic;
using TL7.Core;
using TL7.Movement;
using TL7.Attributes;
using TL7.Stats;
using UnityEngine;
using UnityEngine.AddressableAssets;
using GameDevTV.Saving;
using GameDevTV.Inventories;
using System;

namespace TL7.Combat
{
    [RequireComponent(typeof(ActionScheduler))]
    [RequireComponent(typeof(SaveableEntity))]
    public class Fighter : MonoBehaviour, IAction, ISaveable
    {
        [SerializeField] WeaponConfig defaultWeaponConfig = null;
        [SerializeField] Transform leftHand = null;
        [SerializeField] Transform rightHand = null;
        private Mover mover;
        private Health target;
        private BaseStats stats;
        private Animator animator;
        private WeaponConfig currentWeaponConfig;
        private ActionScheduler actionScheduler;
        private Weapon currentWeapon;
        private float timeSinceLastAttack = Mathf.Infinity;
        private Equipment equipment;

        public Health Target => target;
        public WeaponConfig CurrentWeaponConfig => currentWeaponConfig;
        public Transform Hand => currentWeaponConfig.IsLeftHanded ? leftHand : rightHand;


        private bool IsInWeaponRange
        {
            get
            {
                if ((target != null) && (currentWeaponConfig != null))
                {
                    float distance = Vector3.Distance(transform.position, target.transform.position);
                    return distance <= currentWeaponConfig.Range;
                }
                return false;
            }
        }

        void Awake()
        {
            mover = GetComponent<Mover>();
            stats = GetComponent<BaseStats>();
            animator = GetComponent<Animator>();
            actionScheduler = GetComponent<ActionScheduler>();
            equipment = GetComponent<Equipment>();
        }

        void OnEnable()
        {
            if (equipment)
            {
                equipment.equipmentUpdated += UpdateWeapon;
            }
        }

        void OnDisable()
        {
            if (equipment)
            {
                equipment.equipmentUpdated -= UpdateWeapon;
            }
        }

        private void UpdateWeapon()
        {
            var weaponConfig = equipment?.GetItemInSlot(EquipLocation.Weapon) as WeaponConfig;
            Equip(weaponConfig ?? defaultWeaponConfig);
        }

        void Start()
        {
            if (currentWeaponConfig == null)
            {
                equipment?.AddItem(EquipLocation.Weapon, defaultWeaponConfig);
                Equip(defaultWeaponConfig);
            }
        }

        private void Equip(in WeaponConfig weaponConfig)
        {
            if (weaponConfig != null)
            {
                if ((currentWeaponConfig != null) && (currentWeaponConfig.PrefabName == weaponConfig.PrefabName))
                {
                    return;
                }
                currentWeaponConfig = weaponConfig;
                Destroy(currentWeapon?.gameObject);
                currentWeapon = currentWeaponConfig.Spawn(owner: this, animator);
            }
        }

        void Update()
        {
            timeSinceLastAttack += Time.deltaTime;
            if (target != null && !target.IsDead)
            {
                if (IsInWeaponRange)
                {
                    mover.CancelAction();
                    PerformAttackTasks();
                }
                else
                {
                    mover.MoveTo(target.transform.position, 1f);
                }
            }
        }

        private void PerformAttackTasks()
        {
            transform.LookAt(target.transform);
            if (timeSinceLastAttack > currentWeaponConfig.TimeBetweenAttacks)
            {
                // Hit event is triggered at specified interval within the animation
                SwitchTriggers(triggerToReset: "stopAttacking", triggerToSet: "attack");
                timeSinceLastAttack = 0f;
            }
        }

        // Animator event callback
        private void AnimatorHitTriggered()
        {
            float damage = stats.Of(StatType.Damage);

            currentWeapon?.OnHit();

            if (!currentWeaponConfig.TryLaunchProjectile(owner: this, target, damage) && IsInWeaponRange)
            {
                target?.TakeDamage(damage, instigator: this);
            }
        }

        // Synonymous. Called if specific named callback is requested.
        void Hit()
        {
            AnimatorHitTriggered();
        }

        void Shoot()
        {
            AnimatorHitTriggered();
        }

        public bool CanTarget(in GameObject targetObject)
        {
            var testTarget = targetObject?.GetComponent<Health>();
            bool validAndAlive = (testTarget != null) && !testTarget.IsDead;
            return validAndAlive && mover.CanMoveTo(testTarget.transform.position);
        }

        public void StartAttackAction(in GameObject targetObject)
        {
            actionScheduler.StartAction(this);
            this.target = targetObject.GetComponent<Health>();
        }

        public void StartAction(params object[] args) => StartAttackAction(args[0] as GameObject);

        public void CancelAction()
        {
            SwitchTriggers(triggerToReset: "attack", triggerToSet: "stopAttacking");
            target = null;
            mover.CancelAction();
        }

        private void SwitchTriggers(in string triggerToReset, in string triggerToSet)
        {
            animator.ResetTrigger(triggerToReset);
            animator.SetTrigger(triggerToSet);
        }

        object ISaveable.CaptureState()
        {
            return (currentWeaponConfig ?? defaultWeaponConfig).GetItemID();
        }

        void ISaveable.RestoreState(object state)
        {
            Equip(InventoryItem.GetFromID((string)state) as WeaponConfig);
        }
    }
}
