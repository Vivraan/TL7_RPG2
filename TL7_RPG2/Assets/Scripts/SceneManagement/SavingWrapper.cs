﻿using System.Collections;
using System.IO;
using GameDevTV.Saving;
using UnityEngine;

namespace TL7.SceneManagement
{
    public class SavingWrapper : MonoBehaviour
    {
        private const string saveHotKey = "Save";
        private const string loadHotKey = "Load";
        private const string resetHotKey = "Delete Save";

        [SerializeField] string saveFileName = "tl7-save";
        private string SaveFilePath => saveFileName;
        private SavingSystem savingSystem;

        void Awake()
        {
            savingSystem = GetComponent<SavingSystem>();
        }

        IEnumerator Start()
        {
            yield return LoadCheckpoint();
            Save();
        }

        void Update()
        {
            if (Input.GetAxis(saveHotKey) > 0f)
            {
                Save();
            }
            if (Input.GetAxis(loadHotKey) > 0f)
            {
                Load();
            }
            if (Input.GetAxis(resetHotKey) > 0f)
            {
                ResetSaveFile();
            }
        }

        public IEnumerator LoadCheckpoint()
        {
            var fader = FindObjectOfType<Fader>();
            fader.FadeOutImmediate();
            yield return savingSystem.LoadLastScene(SaveFilePath);
            yield return fader.FadeIn();
        }

        public void Save()
        {
            savingSystem.Save(SaveFilePath);
        }

        public void Load()
        {
            print($"Loading from {SaveFilePath}");
            savingSystem.Load(SaveFilePath);
        }

        private void ResetSaveFile()
        {
            print($"Deleting save file at {SaveFilePath}");
            savingSystem.Delete(SaveFilePath);
        }
    }
}
