﻿using System;
using GameDevTV.Saving;
using UnityEngine;

namespace TL7.Attributes
{
    [RequireComponent(typeof(SaveableEntity))]
    public class Experience : MonoBehaviour, ISaveable
    {
        public event Action XPClaimed;

        private float experiencePoints = 0f;
        public float XP => experiencePoints;

        public void AwardXP(in float xp)
        {
            experiencePoints += xp;
            XPClaimed();
        }

        object ISaveable.CaptureState()
        {
            return experiencePoints;
        }

        void ISaveable.RestoreState(object state)
        {
            experiencePoints = (float)state;
        }
    }
}
