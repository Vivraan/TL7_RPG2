﻿using TL7.Saving;
using UnityEngine;

namespace TL7.Attributes
{
    public class HealthBar : MonoBehaviour
    {
        [SerializeField] Health health = null;
        [SerializeField] Canvas canvas = null;
        [SerializeField] RectTransform greenBar = null;

        void Update()
        {
            if (health.IsDead || Mathf.Approximately(health.HitPoints, health.MaxHitPoints))
            {
                canvas.enabled = false;
            }
            else
            {
                canvas.enabled = true;
                greenBar.localScale = new Vector3(health.Fraction, 1, 1);
            }
        }
    }
}
