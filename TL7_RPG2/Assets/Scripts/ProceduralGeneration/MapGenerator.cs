﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace TL7.ProceduralGeneration
{
    public enum TileType
    {
        Room, Wall
    };

    public class MapGenerator : MonoBehaviour
    {
        [SerializeField] Vector2Int dimensions = Vector2Int.zero;
        [SerializeField, Range(0f, 1f)] float fillFraction = .45f;
        [SerializeField] string seed = string.Empty;
        [SerializeField] bool useTimeSeed = false;
        [SerializeField, Range(1, 20)] int mapSmoothingIterations = 5;
        [SerializeField, Range(4, 6)] int maxWallNeighbours = 4;
        [SerializeField, Range(0, 1000)] int maxWallRegionSize = 0;
        [SerializeField, Range(0, 1000)] int maxRoomRegionSize = 0;
        [SerializeField] float meshSquareSize = 1f;
        [SerializeField, Range(0, 20)] int borderSize = 5;
        private TileType[,] map;

        void Start()
        {
            Generate();
        }

        void Update()
        {
            if (Input.GetMouseButtonDown(0))
            {
                Generate();
            }
        }

        public void Generate()
        {
            map = new TileType[dimensions.x, dimensions.y];

            FillMapRandom();

            for (int i = 0; i < mapSmoothingIterations; ++i)
            {
                SmoothenMap(maxWallNeighbours);
            }

            ProcessRooms(maxWallRegionSize, maxRoomRegionSize);

            var borderedMap = new TileType[dimensions.x + (borderSize * 2), dimensions.y + (borderSize * 2)];

            for (int x = 0; x < borderedMap.GetLength(0); ++x)
            {
                for (int y = 0; y < borderedMap.GetLength(1); ++y)
                {
                    if (IsInBorderZone(x, y))
                    {
                        borderedMap[x, y] = map[x - borderSize, y - borderSize];
                    }
                    else
                    {
                        borderedMap[x, y] = TileType.Wall;
                    }
                }
            }

            GetComponent<MeshGenerator>().Generate(borderedMap, meshSquareSize);
        }


        public void FillMapRandom()
        {
            if (map != null)
            {
                if (useTimeSeed)
                {
                    seed = $"{Time.time}";
                }
                UnityEngine.Random.InitState(seed.GetHashCode());

                for (int x = 0; x < dimensions.x; ++x)
                {
                    for (int y = 0; y < dimensions.y; ++y)
                    {
                        if (IsOnBoundary(x, y))
                        {
                            map[x, y] = TileType.Wall;
                        }
                        else
                        {
                            map[x, y] = (UnityEngine.Random.Range(0f, 1f) < fillFraction) ? TileType.Wall : TileType.Room;
                        }
                    }
                }
            }
        }

        private void SmoothenMap(in int wallNeighbourLimit)
        {
            // Depending on the iteration order, for a higher neighbour limit,
            // a bias is expressed in the choice of empty spaces
            for (int x = 0; x < dimensions.x; ++x)
            {
                for (int y = 0; y < dimensions.y; ++y)
                {
                    // change the given cell based on its neighbours
                    int wallNeighbourCount = SurroundingWallsCount(x, y);

                    if (wallNeighbourCount > wallNeighbourLimit)
                    {
                        map[x, y] = TileType.Wall;
                    }
                    else if (wallNeighbourCount < wallNeighbourLimit)
                    {
                        map[x, y] = TileType.Room;
                    }
                    // remains unchanged otherwise
                }
            }
        }

        private int SurroundingWallsCount(in int gridX, in int gridY)
        {
            int count = 0;
            for (int neighbourX = gridX - 1; neighbourX <= gridX + 1; ++neighbourX)
            {
                for (int neighbourY = gridY - 1; neighbourY <= gridY + 1; ++neighbourY)
                {
                    if (IsInsideMap(neighbourX, neighbourY))
                    {
                        if ((neighbourX != gridX) || (neighbourY != gridY))
                        {
                            count += (int)map[neighbourX, neighbourY];
                        }
                    }
                    else
                    {
                        count++;
                    }
                }
            }
            return count;
        }

        private void ProcessRooms(in int wallSizeThreshold, in int roomSizeThreshold)
        {
            var wallRegions = GetRegions(TileType.Wall);

            foreach (var wallRegion in wallRegions)
            {
                if (wallRegion.Count < wallSizeThreshold)
                {
                    foreach (var tile in wallRegion)
                    {
                        map[tile.x, tile.y] = TileType.Room;
                    }
                }
            }

            var roomRegions = GetRegions(TileType.Room);
            var survivingRooms = new List<Room>();

            foreach (var roomRegion in roomRegions)
            {
                if (roomRegion.Count < roomSizeThreshold)
                {
                    foreach (var tile in roomRegion)
                    {
                        map[tile.x, tile.y] = TileType.Wall;
                    }
                }
                else
                {
                    survivingRooms.Add(new Room(roomRegion, map));
                }
            }
            ConnectClosestRooms(survivingRooms);
        }

        void ConnectClosestRooms(in List<Room> allRooms)
        {
            int bestDistance = 0;
            (int x, int y) bestTileA = (0, 0), bestTileB = (0, 0);
            Room bestRoomA = null;
            Room bestRoomB = null;
            bool possibleConnectionFound = false;

            foreach (var roomA in allRooms)
            {
                possibleConnectionFound = false;

                foreach (var roomB in allRooms)
                {
                    if (roomA.IsConnected(roomB))
                    {
                        possibleConnectionFound = false;
                        break;
                    }
                    if (roomA != roomB)
                    {
                        for (int tileIndexA = 0; tileIndexA < roomA.edgeTiles.Count; ++tileIndexA)
                        {
                            for (int tileIndexB = 0; tileIndexB < roomB.edgeTiles.Count; ++tileIndexB)
                            {
                                var tileA = roomA.edgeTiles[tileIndexA];
                                var tileB = roomB.edgeTiles[tileIndexB];

                                int del_x = tileA.x - tileB.x;
                                int del_y = tileA.y - tileB.y;
                                int sqDistanceBetweenRooms =  (del_x * del_x) + (del_y * del_y);
                                if (sqDistanceBetweenRooms < bestDistance || !possibleConnectionFound)
                                {
                                    bestDistance = sqDistanceBetweenRooms;
                                    possibleConnectionFound = true;
                                    bestTileA = tileA;
                                    bestTileB = tileB;
                                    bestRoomA = roomA;
                                    bestRoomB = roomB;
                                }
                            }
                        }
                    }
                }

                if (possibleConnectionFound)
                {
                    CreatePassage(bestRoomA, bestRoomB, bestTileA, bestTileB);
                }
            }
        }

        private void CreatePassage(in Room roomA, in Room roomB, in (int x, int y) tileA, in (int x, int y) tileB)
        {
            Room.ConnectRooms(roomA, roomB);
            Debug.DrawLine(TileCoordToWorldPoint(tileA), TileCoordToWorldPoint(tileB), Color.green, 10f);
        }

        private Vector3 TileCoordToWorldPoint(in (int x, int y) tile)
        {
            return new Vector3((-dimensions.x / 2) + .5f + tile.x, 3f, (-dimensions.y / 2) + .5f + tile.y);
        }

        List<List<(int x, int y)>> GetRegions(in TileType tileType)
        {
            var regions = new List<List<(int x, int y)>>();
            var mapFlags = new int[dimensions.x, dimensions.y];

            for (int x = 0; x < dimensions.x; ++x)
            {
                for (int y = 0; y < dimensions.y; ++y)
                {
                    if ((mapFlags[x, y] == 0) && (map[x, y] == tileType))
                    {
                        var newRegion = GetRegionTiles(x, y);
                        regions.Add(newRegion);

                        foreach (var tile in newRegion)
                        {
                            mapFlags[tile.x, tile.y] = 1;
                        }
                    }
                }
            }
            return regions;
        }

        private List<(int x, int y)> GetRegionTiles(in int startX, in int startY)
        {
            var tiles = new List<(int x, int y)>();
            var mapFlags = new int[dimensions.x, dimensions.y];
            var tileType = map[startX, startY];

            var queue = new Queue<(int x, int y)>();
            queue.Enqueue((startX, startY));
            mapFlags[startX, startY] = 1;

            while (queue.Count > 0)
            {
                var tile = queue.Dequeue();
                tiles.Add(tile);

                for (int x = tile.x - 1; x <= tile.x + 1; ++x)
                {
                    for (int y = tile.y - 1; y <= tile.y + 1; ++y)
                    {
                        if (IsInsideMap(x, y) && ((y == tile.y) || (x == tile.x)))
                        {
                            if ((mapFlags[x, y] == 0) && (map[x, y] == tileType))
                            {
                                mapFlags[x, y] = 1;
                                queue.Enqueue((x, y));
                            }
                        }
                    }
                }
            }

            return tiles;
        }

        private bool IsOnBoundary(int x, int y)
        {
            return (x == 0) || (x == dimensions.x - 1) || (y == 0) || (y == dimensions.y - 1);
        }

        private bool IsInsideMap(int x, int y)
        {
            return (x >= 0) && (x < dimensions.x) && (y >= 0) && (y < dimensions.y);
        }

        private bool IsInBorderZone(int x, int y)
        {
            return ((x >= borderSize) && (x < dimensions.x + borderSize) && (y >= borderSize) && (y < dimensions.y + borderSize));
        }

        class Room
        {
            public List<(int x, int y)> tiles;
            public List<(int x, int y)> edgeTiles;
            public List<Room> connectedRooms;
            public int roomSize;

            public Room() { }

            public Room(in List<(int x, int y)> region, in TileType[,] map)
            {
                tiles = region;
                roomSize = tiles.Count;
                connectedRooms = new List<Room>();

                edgeTiles = new List<(int x, int y)>();
                foreach (var tile in tiles)
                {
                    for (int x = tile.x - 1; x <= tile.x + 1; ++x)
                    {
                        for (int y = tile.y - 1; y <= tile.y + 1; ++y)
                        {
                            // including diagonals
                            if ((x == tile.x) || (y == tile.y))
                            {
                                if (map[x, y] == TileType.Wall)
                                {
                                    edgeTiles.Add(tile);
                                }
                            }
                        }
                    }
                }
            }

            public static void ConnectRooms(in Room a, in Room b)
            {
                a.connectedRooms.Add(b);
                b.connectedRooms.Add(a);
            }

            public bool IsConnected(in Room other)
            {
                return connectedRooms.Contains(other);
            }
        }

        // void OnDrawGizmos()
        // {
        //     if (map != null)
        //     {
        //         for (int x = 0; x < dimensions.x; ++x)
        //         {
        //             for (int y = 0; y < dimensions.y; ++y)
        //             {
        //                 Gizmos.color = (map[x, y] == 1) ? Color.black : Color.white;
        //                 var position = new Vector3((-dimensions.x / 2) + x + .5f, 0, (-dimensions.y / 2) + y + .5f);
        //                 Gizmos.DrawCube(position, Vector3.one);
        //             }
        //         }
        //     }    
        // }
    }
}