﻿using System.Collections.Generic;
using UnityEngine;

namespace TL7.ProceduralGeneration
{
    public class MeshGenerator : MonoBehaviour
    {
        [SerializeField] MeshFilter walls = null;
        [SerializeField] float wallHeight = 5f;
        public SquareGrid squareGrid;
        List<Vector3> vertices = null;
        List<int> triangles = null;
        
        Dictionary<int, List<Triangle>> trianglesDict = new Dictionary<int, List<Triangle>>();
        List<List<int>> outlines = new List<List<int>>();
        HashSet<int> checkedVertices = new HashSet<int>();

        public void Generate(in TileType[,] map, in float squareSize)
        {
            outlines.Clear();
            checkedVertices.Clear();
            trianglesDict.Clear();

            squareGrid = new SquareGrid(map, squareSize);
            vertices = new List<Vector3>();
            triangles = new List<int>();

            for (int x = 0; x < squareGrid.squares.GetLength(0); ++x)
            {
                for (int y = 0; y < squareGrid.squares.GetLength(1); ++y)
                {
                    TriangulateSquare(squareGrid.squares[x, y]);
                }
            }

            var mesh = new Mesh();
            GetComponent<MeshFilter>().mesh = mesh;
            mesh.vertices = vertices.ToArray();
            mesh.triangles = triangles.ToArray();
            mesh.RecalculateNormals();
            mesh.Optimize();

            CreateWallMesh();
        }

        private void CreateWallMesh()
        {
            CalculateOutlines();

            var wallVertices = new List<Vector3>();
            var wallTriangles = new List<int>();

            var wallMesh = new Mesh();
            
            foreach(var outline in outlines)
            {
                for (int i = 0; i < outline.Count - 1; ++i)
                {
                    int startIndex = wallVertices.Count;
                    wallVertices.Add(vertices[outline[i]]);
                    wallVertices.Add(vertices[outline[i + 1]]);
                    wallVertices.Add(vertices[outline[i]] - (Vector3.up * wallHeight));
                    wallVertices.Add(vertices[outline[i + 1]] - (Vector3.up * wallHeight));

                    // Note: order is important for assigning the "handedness" of the triangle
                    wallTriangles.Add(startIndex + 0);
                    wallTriangles.Add(startIndex + 2);
                    wallTriangles.Add(startIndex + 3);

                    wallTriangles.Add(startIndex + 3);
                    wallTriangles.Add(startIndex + 1);
                    wallTriangles.Add(startIndex + 0);
                }
            }

            walls.mesh = wallMesh;
            wallMesh.vertices = wallVertices.ToArray();
            wallMesh.triangles = wallTriangles.ToArray();
            wallMesh.RecalculateNormals();
            wallMesh.Optimize();
        }

        private void TriangulateSquare(in Square square)
        {
            switch (square.configuration)
            {
                case 0:
                    break;

                // 1 active ControlNode
                // note: order is important for ensuring the "handedness" of the edge
                case 1:
                    MeshFromPoints(square.centreLeft, square.centreBottom, square.bottomLeft);
                    break;

                case 2:
                    MeshFromPoints(square.bottomRight, square.centreBottom, square.centreRight);
                    break;

                case 4:
                    MeshFromPoints(square.topRight, square.centreRight, square.centreTop);
                    break;

                case 8:
                    MeshFromPoints(square.topLeft, square.centreTop, square.centreLeft);
                    break;

                // 2 active ControlNodes
                // adjacent
                case 3:
                    MeshFromPoints(square.centreRight, square.bottomRight, square.bottomLeft, square.centreLeft);
                    break;

                case 6:
                    MeshFromPoints(square.centreTop, square.topRight, square.bottomRight, square.centreBottom);
                    break;

                case 9:
                    MeshFromPoints(square.topLeft, square.centreTop, square.centreBottom, square.bottomLeft);
                    break;

                case 12:
                    MeshFromPoints(square.topLeft, square.topRight, square.centreRight, square.centreLeft);
                    break;

                // diagonal
                case 5:
                    MeshFromPoints(square.centreTop, square.topRight, square.centreRight, square.centreBottom, square.bottomLeft, square.centreLeft);
                    break;

                case 10:
                    MeshFromPoints(square.topLeft, square.centreTop, square.centreRight, square.bottomRight, square.centreBottom, square.centreLeft);
                    break;

                // 3 active ControlNodes
                case 7:
                    MeshFromPoints(square.centreTop, square.topRight, square.bottomRight, square.bottomLeft, square.centreLeft);
                    break;

                case 11:
                    MeshFromPoints(square.topLeft, square.centreTop, square.centreRight, square.bottomRight, square.bottomLeft);
                    break;

                case 13:
                    MeshFromPoints(square.topLeft, square.topRight, square.centreRight, square.centreBottom, square.bottomLeft);
                    break;

                case 14:
                    MeshFromPoints(square.topLeft, square.topRight, square.bottomRight, square.centreBottom, square.centreLeft);
                    break;

                // All ControlNodes active
                case 15:
                    MeshFromPoints(square.topLeft, square.topRight, square.bottomRight, square.bottomLeft);
                    // These are never part of any walls. Safely add them to checkedVertices.
                    checkedVertices.Add(square.topLeft.vertexIndex);
                    checkedVertices.Add(square.topRight.vertexIndex);
                    checkedVertices.Add(square.bottomRight.vertexIndex);
                    checkedVertices.Add(square.bottomLeft.vertexIndex);
                    break;
            }
        }

        private void MeshFromPoints(params Node[] points)
        {
            AssignVertices(points);

            // Kinda exempliefies why the order is important
            if (points.Length >= 3)
            {
                CreateTriangle(points[0], points[1], points[2]);
            }
            if (points.Length >= 4)
            {
                CreateTriangle(points[0], points[2], points[3]);
            }
            if (points.Length >= 5)
            {
                CreateTriangle(points[0], points[3], points[4]);
            }
            if (points.Length >= 6)
            {
                CreateTriangle(points[0], points[4], points[5]);
            }
        }

        private void AssignVertices(in Node[] points)
        {
            for (int i = 0; i < points.Length; ++i)
            {
                if (points[i].vertexIndex == -1)
                {
                    // Increments count, so all vertices are assigned starting from 0
                    points[i].vertexIndex = vertices.Count;
                    vertices.Add(points[i].position);
                }
            }
        }

        private void CreateTriangle(in Node a, in Node b, in Node c)
        {
            triangles.Add(a.vertexIndex);
            triangles.Add(b.vertexIndex);
            triangles.Add(c.vertexIndex);

            var triangle = new Triangle(a.vertexIndex, b.vertexIndex, c.vertexIndex);

            RecordTriangle(a.vertexIndex, triangle);
            RecordTriangle(b.vertexIndex, triangle);
            RecordTriangle(c.vertexIndex, triangle);
        }

        private void CalculateOutlines()
        {
            for (int vertexIndex = 0; vertexIndex < vertices.Count; ++vertexIndex)
            {
                if (!checkedVertices.Contains(vertexIndex))
                {
                    int newOutlineVertex = GetConnectedOutlineVertex(vertexIndex);
                    
                    if (newOutlineVertex != -1)
                    {
                        checkedVertices.Add(vertexIndex);
                        
                        var newOutline = new List<int>();
                        newOutline.Add(vertexIndex);
                        outlines.Add(newOutline);
                        FollowOutline(newOutlineVertex, outlines.Count - 1);
                        outlines[outlines.Count - 1].Add(vertexIndex);
                    }
                }
            }
        }

        private void FollowOutline(int vertexIndex, int outlineIndex)
        {
            outlines[outlineIndex].Add(vertexIndex);
            checkedVertices.Add(vertexIndex);
            
            int nextVertexIndex = GetConnectedOutlineVertex(vertexIndex);
            if (nextVertexIndex != -1)
            {
                FollowOutline(nextVertexIndex, outlineIndex);
            }
        }

        private int GetConnectedOutlineVertex(int vertexA)
        {
            var trianglesWithA = trianglesDict[vertexA];

            for (int i = 0; i < trianglesWithA.Count; ++i)
            {
                var triangle = trianglesWithA[i];

                for (int j = 0; j < 3; ++j)
                {
                    int vertexB = triangle.vertices[j];

                    if ((vertexB != vertexA) && !checkedVertices.Contains(vertexB) && IsOutlineEdge(vertexA, vertexB))
                    {
                        return vertexB;
                    }
                }
            }

            return -1;
        }

        bool IsOutlineEdge(in int vertexA, in int vertexB)
        {
            var trianglesWithA = trianglesDict[vertexA];
            int sharedTriangleCount = 0;

            for (int i = 0; i < trianglesWithA.Count; ++i)
            {
                if (trianglesWithA[i].Contains(vertexB))
                {
                    sharedTriangleCount++;
                    if (sharedTriangleCount > 1)
                    {
                        break;
                    }
                }
            }

            // vertices share exactly one triangle
            return (sharedTriangleCount == 1);
        }


        private void RecordTriangle(in int vertexKey, in Triangle triangle)
        {
            if (trianglesDict.ContainsKey(vertexKey))
            {
                trianglesDict[vertexKey].Add(triangle);
            }
            else
            {
                trianglesDict.Add(vertexKey, new List<Triangle> { triangle });
            }
        }

        // void OnDrawGizmos()
        // {
        //     if (squareGrid != null)
        //     {
        //         for (int x = 0; x < squareGrid.squares.GetLength(0); ++x)
        //         {
        //             for (int y = 0; y < squareGrid.squares.GetLength(1); ++y)
        //             {
        //                 Gizmos.color = squareGrid.squares[x, y].topLeft.active ? Color.black : Color.white;
        //                 Gizmos.DrawCube(squareGrid.squares[x, y].topLeft.position, Vector3.one * .4f);

        //                 Gizmos.color = squareGrid.squares[x, y].topRight.active ? Color.black : Color.white;
        //                 Gizmos.DrawCube(squareGrid.squares[x, y].topRight.position, Vector3.one * .4f);

        //                 Gizmos.color = squareGrid.squares[x, y].bottomRight.active ? Color.black : Color.white;
        //                 Gizmos.DrawCube(squareGrid.squares[x, y].bottomRight.position, Vector3.one * .4f);

        //                 Gizmos.color = squareGrid.squares[x, y].bottomLeft.active ? Color.black : Color.white;
        //                 Gizmos.DrawCube(squareGrid.squares[x, y].bottomLeft.position, Vector3.one * .4f);

        //                 Gizmos.color = Color.grey;
        //                 Gizmos.DrawCube(squareGrid.squares[x, y].centreTop.position, Vector3.one * .15f);
        //                 Gizmos.DrawCube(squareGrid.squares[x, y].centreRight.position, Vector3.one * .15f);
        //                 Gizmos.DrawCube(squareGrid.squares[x, y].centreBottom.position, Vector3.one * .15f);
        //                 Gizmos.DrawCube(squareGrid.squares[x, y].centreLeft.position, Vector3.one * .15f);
        //             }
        //         }
        //     }
        // }

        class Triangle
        {
            public int[] vertices = new int[3];

            public Triangle(in int vertexA, in int vertexB, in int vertexC)
            {
                vertices[0] = vertexA;
                vertices[1] = vertexB;
                vertices[2] = vertexC;
            }

            public bool Contains(in int vertexIndex)
            {
                return (vertexIndex == vertices[0]) || (vertexIndex == vertices[1]) || (vertexIndex == vertices[2]);
            }
        }

        /*
            ...      ...
            1 == A == 2 ~~~~> ControlNode is a Node
            \\       \\
            B         C ~~~~~> Node
            \\        \\
                4 == D == 3 == ...

            This is a Square, and the ways in which one can join
            lines between each of the nodes to form triangles are
            restricted to 16 (permuting the activation of each ControlNode
            yields 2^4 possibilities, stored as a 4-bit configuration).

            ControlNodes know which nodes are above them and to their right.
            Squares know about the ControlNodes and Nodes that form them.
        */

        public class SquareGrid
        {
            public Square[,] squares;

            public SquareGrid(in TileType[,] map, in float squareSize)
            {
                int nodeCountX = map.GetLength(0);
                int nodeCountY = map.GetLength(1);
                float mapWidth = nodeCountX * squareSize;
                float mapHeight = nodeCountY * squareSize;

                var controlNodes = new ControlNode[nodeCountX, nodeCountY];
                for (int x = 0; x < nodeCountX; ++x)
                {
                    for (int y = 0; y < nodeCountY; ++y)
                    {
                        var position = new Vector3(
                            // centred around each square
                            (-mapWidth / 2) + (x * squareSize) + (squareSize / 2),
                            0,
                            (-mapHeight / 2) + (y * squareSize) + (squareSize / 2));
                        controlNodes[x, y] = new ControlNode(position, (map[x, y] == TileType.Wall), squareSize);
                    }
                }

                squares = new Square[nodeCountX - 1, nodeCountY - 1];
                for (int x = 0; x < nodeCountX - 1; ++x)
                {
                    for (int y = 0; y < nodeCountY - 1; ++y)
                    {
                        squares[x, y] = new Square(
                            topLeft: controlNodes[x, y + 1],
                            topRight: controlNodes[x + 1, y + 1],
                            bottomRight: controlNodes[x + 1, y],
                            bottomLeft: controlNodes[x, y]);
                    }
                }
            }
        }

        public class Square
        {
            public ControlNode topLeft, topRight, bottomRight, bottomLeft;
            public Node centreTop, centreRight, centreBottom, centreLeft;
            public int configuration;

            public Square(in ControlNode topLeft, in ControlNode topRight, in ControlNode bottomRight, in ControlNode bottomLeft)
            {
                this.topLeft = topLeft;
                this.topRight = topRight;
                this.bottomLeft = bottomLeft;
                this.bottomRight = bottomRight;

                centreTop = topLeft.right;
                centreRight = bottomRight.above;
                centreBottom = bottomLeft.right;
                centreLeft = bottomLeft.above;

                // Following a clockwise order for assigning a bit for each ControlNode
                if (topLeft.active)
                {
                    configuration += 8;
                }
                if (topRight.active)
                {
                    configuration += 4;
                }
                if (bottomRight.active)
                {
                    configuration += 2;
                }
                if (bottomLeft.active)
                {
                    configuration += 1;
                }
            }
        }

        public class Node
        {
            public Vector3 position;
            public int vertexIndex = -1;

            public Node(in Vector3 position)
            {
                this.position = position;
            }
        }

        public class ControlNode : Node
        {
            public bool active;
            public Node above, right;

            public ControlNode(in Vector3 position, in bool active, in float squareSize) : base(position)
            {
                this.active = active;
                above = new Node(position + (Vector3.forward * squareSize / 2f));
                right = new Node(position + (Vector3.right * squareSize / 2f));
            }
        }
    }
}
