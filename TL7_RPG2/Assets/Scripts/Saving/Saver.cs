﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace TL7.Saving
{
    [ExecuteAlways]
    public class Saver : MonoBehaviour
    {
        [SerializeField] string statesGuidOverride = string.Empty;
        [Tooltip("Please use States Guid Override for setting a custom GUID.")]
        [SerializeField] string statesGuid = string.Empty;
        public string StatesGuid => statesGuid;

        static Dictionary<string, Saver> globalLookup = new Dictionary<string, Saver>();

        public object ComponentStates
        {
            get
            {
                var states = new Dictionary<Type, object>();
                foreach (var saveable in GetComponents<ISaveable>())
                {
                    states[saveable.GetType()] = saveable.CaptureState();
                }
                return states;
            }

            set
            {
                var states = value as Dictionary<Type, object>;
                foreach (var saveable in GetComponents<ISaveable>())
                {
                    if (states.ContainsKey(saveable.GetType()))
                    {
                        saveable.RestoreState(states[saveable.GetType()]);
                    }
                }
            }
        }

        void Awake()
        {
            if (!string.IsNullOrEmpty(statesGuidOverride))
            {
                statesGuid = statesGuidOverride;
            }
        }

        void Update()
        {
            bool inSceneViewport = !string.IsNullOrEmpty(gameObject.scene.path);
            if (!Application.IsPlaying(gameObject) && inSceneViewport)
            {
#if (UNITY_EDITOR)
                // TODO If a private statesGuid is used, remove this code
                var serializedObject = new SerializedObject(this);
                var guidProperty = serializedObject.FindProperty(nameof(statesGuid));

                if (string.IsNullOrEmpty(guidProperty.stringValue) || !IsUniqueGuid(guidProperty.stringValue))
                {
                    guidProperty.stringValue = System.Guid.NewGuid().ToString();
                    serializedObject.ApplyModifiedProperties();
                }

                globalLookup[guidProperty.stringValue] = this;
#else
            
                if (string.IsNullOrEmpty(statesGuid) || !IsUniqueGuid(statesGuid))
                {
                    statesGuid = System.Guid.NewGuid().ToString();
                }

                globalLookup[statesGuid] = this;
#endif
            }
        }

        private bool IsUniqueGuid(in string candidate)
        {
            if (!globalLookup.ContainsKey(candidate) || globalLookup[candidate] == this)
            {
                return true;
            }

            // Upon refreshing a scene view, allow a fresh guid to replace this one
            if (globalLookup[candidate] == null || globalLookup[candidate].statesGuid != candidate)
            {
                globalLookup.Remove(candidate);
                return true;
            }

            return false;
        }
    }
}
