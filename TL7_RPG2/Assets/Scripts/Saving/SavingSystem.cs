﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace TL7.Saving
{
    public class SavingSystem : MonoBehaviour
    {
        public IEnumerator LoadLastScene(string path)
        {
            var gameState = LoadGameStateFromFile(path);
            int lastSceneBuildIndex = SceneManager.GetActiveScene().buildIndex;
            if (gameState.ContainsKey(nameof(lastSceneBuildIndex)))
            {
                lastSceneBuildIndex = (int)gameState[nameof(lastSceneBuildIndex)];
                if (lastSceneBuildIndex != SceneManager.GetActiveScene().buildIndex)
                {
                    yield return SceneManager.LoadSceneAsync(lastSceneBuildIndex);
                }
            }
            RestoreGameState(gameState);
        }

        public void Save(in string path)
        {
            print($"Saving to {path}");
            Dictionary<string, object> gameState = LoadGameStateFromFile(path);
            UpdateGameState(ref gameState);
            SaveGameStateToFile(path, gameState);
        }

        public void Load(in string path)
        {
            print($"Loading from {path}");
            RestoreGameState(LoadGameStateFromFile(path));
        }

        private void SaveGameStateToFile(in string path, in object graph)
        {
            using (FileStream stream = File.Open(path, FileMode.OpenOrCreate))
            {
                new BinaryFormatter().Serialize(stream, graph);
            }
        }

        private Dictionary<string, object> LoadGameStateFromFile(in string path)
        {
            if (File.Exists(path))
            {
                using (FileStream stream = File.Open(path, FileMode.Open))
                {
                    try
                    {
                        return new BinaryFormatter().Deserialize(stream) as Dictionary<string, object>;
                    }
                    catch (System.Exception e)
                    {
                        Debug.Log(
$@"Deserialization error!
{e}
"
                        );
                        return new Dictionary<string, object>();
                    }
                }
            }
            else
            {
                return new Dictionary<string, object>();
            }
        }

        private void UpdateGameState(ref Dictionary<string, object> gameState)
        {
            foreach (var saver in FindObjectsOfType<Saver>())
            {
                gameState[saver.StatesGuid] = saver.ComponentStates;
            }
            gameState["lastSceneBuildIndex"] = SceneManager.GetActiveScene().buildIndex;
        }

        private void RestoreGameState(in Dictionary<string, object> gameState)
        {
            foreach (var saver in FindObjectsOfType<Saver>())
            {
                if (gameState.ContainsKey(saver.StatesGuid))
                {
                    saver.ComponentStates = gameState[saver.StatesGuid];
                }
            }
        }
    }
}
