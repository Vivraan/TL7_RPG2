﻿using System;
using UnityEngine;

namespace TL7.Saving
{
    [Serializable]
    public struct SerializedBasis
    {
        readonly float posX, posY, posZ;
        readonly float rotX, rotY, rotZ, rotW;

        public SerializedBasis(in Vector3 position, in Quaternion rotation)
        {
            posX = position.x;
            posY = position.y;
            posZ = position.z;

            rotX = rotation.x;
            rotY = rotation.y;
            rotZ = rotation.z;
            rotW = rotation.w;
        }

        public Vector3 Position => new Vector3(posX, posY, posZ);
        public Quaternion Rotation => new Quaternion(rotX, rotY, rotZ, rotW);
    }
}
