﻿using TL7.Stats;
using UnityEngine;
using UnityEngine.UI;

namespace TL7.UI
{
    public class LevelDisplay : MonoBehaviour, IHUDText
    {
        [Header("Make sure that the text is a format string.")]
        BaseStats stats;
        Text levelText;
        string prefix;

        string IHUDText.Prefix => prefix;
        Text IHUDText.Text => levelText;

        void Awake()
        {
            stats = GameObject.FindWithTag("Player").GetComponent<BaseStats>();
            levelText = GetComponent<Text>();
            prefix = levelText?.text;
        }

        void Update()
        {
            levelText.text = prefix + stats.Level;
        }
    }
}
