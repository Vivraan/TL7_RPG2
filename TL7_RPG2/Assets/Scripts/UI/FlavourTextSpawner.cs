﻿using UnityEngine;
using UnityEngine.UI;

namespace TL7.UI
{
    public class FlavourTextSpawner : MonoBehaviour
    {
        [SerializeField] FlavourText prefab = null;

        public void Spawn(string displayText)
        {
            var flavourText = Instantiate(prefab, transform);
            flavourText.Text = displayText;
        }
    }
}
