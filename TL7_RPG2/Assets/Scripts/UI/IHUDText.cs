﻿using UnityEngine.UI;

namespace TL7.UI
{
    public interface IHUDText
    {
        string Prefix { get; }
        Text Text { get; }
    }
}