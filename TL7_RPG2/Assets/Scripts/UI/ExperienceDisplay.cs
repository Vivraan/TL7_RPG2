﻿using TL7.Attributes;
using UnityEngine;
using UnityEngine.UI;

namespace TL7.UI
{
    public class ExperienceDisplay : MonoBehaviour, IHUDText
    {
        [Header("Make sure that the text is a format string.")]
        Experience experience;
        Text xpPercentage;
        string prefix;

        string IHUDText.Prefix => prefix;
        Text IHUDText.Text => xpPercentage;

        void Awake()
        {
            experience = GameObject.FindWithTag("Player").GetComponent<Experience>();
            xpPercentage = GetComponent<Text>();
            prefix = xpPercentage?.text;
        }

        void Update()
        {
            xpPercentage.text = prefix + experience.XP;
        }
    }
}
