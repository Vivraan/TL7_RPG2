﻿using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using TL7.Stats;
using UnityEngine;
using UnityEngine.AI;

namespace TL7.Inventories
{
    public class RandomDropper : ItemDropper
    {
        [Tooltip("How far can pickups be scattered from this dropper?")]
        [SerializeField] float scatterDistance = 1;
        [SerializeField] DropLibrary dropLibrary = null;
        private const float NavMeshSampleDistance = 0.1f;
        private const int NumAttempts = 30;

        public void DropRandomStuff()
        {
            var drops = dropLibrary.GetRandomDrops(GetComponent<BaseStats>().Level);
            foreach (var drop in drops)
            {
                DropItem(drop.item, drop.count);
            }
        }

        protected override Vector3 GetDropLocation()
        {
            for (int i = 0; i < NumAttempts; ++i)
            {
                Vector3 randomPoint = transform.position + Random.insideUnitSphere * scatterDistance;
                if (NavMesh.SamplePosition(randomPoint, out NavMeshHit hit, NavMeshSampleDistance, NavMesh.AllAreas))
                {
                    return hit.position;
                }
            }
            return transform.position;
        }
    }
}
