﻿using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using TL7.Stats;
using UnityEngine;

namespace TL7.Inventories
{
    public class StatsEquipment : Equipment, IModifierProvider
    {
        IEnumerable<float> IModifierProvider.AdditivesFor(StatType statType)
        {
            foreach (var slot in GetAllPopulatedSlots())
            {
                var item = GetItemInSlot(slot) as IModifierProvider;
                if (item != null)
                {
                    foreach (float modifier in item.AdditivesFor(statType))
                    {
                        yield return modifier;
                    }
                }
            }
        }

        IEnumerable<float> IModifierProvider.MultipliersFor(StatType statType)
        {
            foreach (var slot in GetAllPopulatedSlots())
            {
                var item = GetItemInSlot(slot) as IModifierProvider;
                if (item != null)
                {
                    foreach (float modifier in item.MultipliersFor(statType))
                    {
                        yield return modifier;
                    }
                }
            }
        }
    }
}
