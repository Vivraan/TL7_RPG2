﻿using System;
using System.Collections;
using System.Collections.Generic;
using GameDevTV.Inventories;
using TL7.Stats;
using UnityEngine;

namespace TL7.Inventories
{
    [CreateAssetMenu(menuName = ("TL7/Inventory/Stats Equipable Item"))]
    public class StatsEquipableItem : EquipableItem, IModifierProvider
    {
        [Header("Stat Modifiers")]
        [SerializeField] Modifier[] additives = null;
        [SerializeField] Modifier[] multipliers = null;

        IEnumerable<float> IModifierProvider.AdditivesFor(StatType statType)
        {
            for (int i = 0; i < additives.Length; i++)
            {
                Modifier additive = additives[i];
                if (additive.statType == statType)
                {
                    yield return additive.value;
                }
            }
        }

        IEnumerable<float> IModifierProvider.MultipliersFor(StatType statType)
        {
            for (int i = 0; i < multipliers.Length; i++)
            {
                Modifier multiplier = multipliers[i];
                if (multiplier.statType == statType)
                {
                    yield return multiplier.value;
                }
            }
        }
    }

    [Serializable]
    struct Modifier
    {
        public StatType statType;
        public float value;
    }
}
